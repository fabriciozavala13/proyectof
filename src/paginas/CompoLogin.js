import React from 'react';


class CompoLogin extends React.Component {
  componentDidMount() {
    // Seleccionando elementos HTML y asignándoles eventos
    document.getElementById("btn__iniciar-sesion").addEventListener("click", this.iniciarSesion);
    document.getElementById("btn__registrarse").addEventListener("click", this.register);
    window.addEventListener("resize", this.anchoPage);
  }

  componentWillUnmount() {
    // Eliminar los eventos al desmontar el componente
    document.getElementById("btn__iniciar-sesion").removeEventListener("click", this.iniciarSesion);
    document.getElementById("btn__registrarse").removeEventListener("click", this.register);
    window.removeEventListener("resize", this.anchoPage);
  }

  anchoPage() {
    const caja_trasera_register = document.querySelector(".caja__trasera-register");
    const caja_trasera_login = document.querySelector(".caja__trasera-login");
    const formulario_login = document.querySelector(".formulario__login");
    const formulario_register = document.querySelector(".formulario__register");
    const contenedor_login_register = document.querySelector(".contenedor__login-register");

    if (window.innerWidth > 850) {
      caja_trasera_register.style.display = "block";
      caja_trasera_login.style.display = "block";
    } else {
      caja_trasera_register.style.display = "block";
      caja_trasera_register.style.opacity = "1";
      caja_trasera_login.style.display = "none";
      formulario_login.style.display = "block";
      contenedor_login_register.style.left = "0px";
      formulario_register.style.display = "none";
    }
  }

  iniciarSesion() {
    const formulario_login = document.querySelector(".formulario__login");
    const formulario_register = document.querySelector(".formulario__register");
    const contenedor_login_register = document.querySelector(".contenedor__login-register");
    const caja_trasera_register = document.querySelector(".caja__trasera-register");
    const caja_trasera_login = document.querySelector(".caja__trasera-login");

    if (window.innerWidth > 850) {
      formulario_login.style.display = "block";
      contenedor_login_register.style.left = "10px";
      formulario_register.style.display = "none";
      caja_trasera_register.style.opacity = "1";
      caja_trasera_login.style.opacity = "0";
    } else {
      formulario_login.style.display = "block";
      contenedor_login_register.style.left = "0px";
      formulario_register.style.display = "none";
      caja_trasera_register.style.display = "block";
      caja_trasera_login.style.display = "none";
    }
  }

  register() {
    const formulario_register = document.querySelector(".formulario__register");
    const contenedor_login_register = document.querySelector(".contenedor__login-register");
    const formulario_login = document.querySelector(".formulario__login");
    const caja_trasera_register = document.querySelector(".caja__trasera-register");
    const caja_trasera_login = document.querySelector(".caja__trasera-login");

    if (window.innerWidth > 850) {
      formulario_register.style.display = "block";
      contenedor_login_register.style.left = "410px";
      formulario_login.style.display = "none";
      caja_trasera_register.style.opacity = "0";
      caja_trasera_login.style.opacity = "1";
    } else {
      formulario_register.style.display = "block";
      contenedor_login_register.style.left = "0px";
      formulario_login.style.display = "none";
      caja_trasera_register.style.display = "none";
      caja_trasera_login.style.display = "block";
      caja_trasera_login.style.opacity = "1";
    }
  }

  validacion(event) {
    event.preventDefault();

    const nombre = document.getElementById('nombre');
    const correo = document.getElementById('correo');
    const usuario = document.getElementById('usuario');
    const contraseña = document.getElementById('contraseña');
    const nuevaClave = document.getElementById('repetir-contra');

    if (nombre.value === '') {
      alert('Por favor ingrese su nombre.');
      return;
    }

    const emailPattern = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    if (!emailPattern.test(correo.value)) {
      alert('Por favor ingrese un correo electrónico válido.');
      return;
    }

    if (usuario.value === '') {
      alert('Por favor ingrese su nombre de usuario.');
      return;
    }

    if (contraseña.value === '') {
      alert('Por favor ingrese una contraseña.');
      return;
    }

    if (contraseña.value !== nuevaClave.value) {
      alert('Las contraseñas no coinciden.');
      return;
    } else {
      alert('Cuenta creada con éxito');
    }
  }

  render() {
    return (
      <main>
        <center>
          <h1 className="titulo">ESTACIONAMIENTO DE LA ULEAM</h1>
        </center>
        <div className="contenedor__todo">
          <div className="caja__trasera">
            <div className="caja__trasera-login">
              <h3>¿Ya tienes una cuenta?</h3>
              <p>Inicia sesión para entrar en la página</p>
              <button id="btn__iniciar-sesion">Iniciar Sesión</button>
            </div>
            <div className="caja__trasera-register">
              <h3>¿Aún no tienes una cuenta?</h3>
              <p>Regístrate para que puedas iniciar sesión</p>
              <button id="btn__registrarse">Regístrarse</button>
            </div>
          </div>

          <div className="contenedor__login-register">
            <form action="" className="formulario__login">
              <h2>Iniciar Sesión</h2>
              <input type="text" placeholder="Correo Electronico" />
              <input type="password" placeholder="Contraseña" />
              <a href='CompoInicio.js'>Iniciar Sesion</a>
            </form>

            <form action="" className="formulario__register" onSubmit={this.validacion}>
              <h2>Regístrarse</h2>
              <input type="text" placeholder="Nombre completo" id="nombre" />
              <input type="text" placeholder="Correo Electronico" id="correo" />
              <input type="text" placeholder="Usuario" id="usuario" />
              <input type="password" placeholder="Contraseña" id="contraseña" />
              <input type="password" placeholder="Repita la contraseña" id="repetir-contra" />
              <button>Regístrarse</button>
            </form>
          </div>
        </div>
      </main>
    );
  }
}

export default CompoLogin;
