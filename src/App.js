import React from 'react';
import './App.css';
import CompoLogin from './paginas/CompoLogin';
import CompoInicio from './paginas/CompoInicio';
import { CompoEyS } from './paginas/CompoEyS';
import { CompoReservar } from './paginas/CompoReservar';
import { CompoEliminar } from './paginas/CompoEliminar';
import { CompoProblema } from './paginas/CompoProblema';

function App() {
  return (
      <div>
        <CompoLogin/>
        <CompoInicio/>
        <CompoEyS/>
        <CompoReservar/>
        <CompoEliminar/>
        <CompoProblema/>
      </div>
    
  );
}

export default App;


