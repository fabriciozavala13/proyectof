import React from 'react';

const CompoInicio = () => {
  return (
    <div>
      <div>
        <header>
          <nav>
            <a href="EyS.html">Registro de E/S</a>
            <a href="Reservar.html">Reservar estacionamiento</a>
            <a href="Eliminar.html">Eliminar reservaciones</a>
            <a href="Mapa.html">¿Cómo llegar?</a>
            <a href="Datos.html">Datos personales</a>
            <a href="Problem.html">Informar un problema</a>
          </nav>
        </header>
      </div>
      <center>
        <div id="centro">
          <main>
            <p>
              ¡Bienvenido al estacionamiento de la Universidad Laica Eloy Alfaro de Manabí (ULEAM)!
              Nuestro objetivo es brindar un servicio seguro, eficiente y accesible para
              estudiantes, profesores y visitantes que necesiten estacionar sus vehículos
              en el campus universitario. Contamos con amplias instalaciones que pueden
              albergar una gran cantidad de vehículos, así como con un personal capacitado
              para asistirlo en lo que necesite. Nos enorgullece ser parte de la comunidad
              universitaria y estamos comprometidos con su bienestar y comodidad.
            </p>
          </main>
        </div>
      </center>
      <div>
        <footer>
          <nav>
            <a href="https://www.uleam.edu.ec/" target="_blank">Uleam</a>
            <a href="https://img.freepik.com/vector-premium/lindo-adorable-hamster-ilustracion-dibujos-animados-animal-bebe-divertido-feliz_746614-74.jpg?w=2000" target="_blank">Alegre</a>
          </nav>
        </footer>
      </div>
    </div>
  );
};

export default CompoInicio;
