import React from 'react';
import './App.css';

const CompoEliminar = () => {
  return (
    <div className="container">
      <h1>Eliminar Reservación de Estacionamiento</h1>
      <form>
        <div className="input-container">
          <label htmlFor="placa">Número de Placa:</label>
          <input type="text" id="placa" name="placa" placeholder="Ingrese la placa del vehículo" required />
        </div>
        <button type="submit">Eliminar Reservación</button>
      </form>
    </div>
  );
};

export default CompoEliminar;
