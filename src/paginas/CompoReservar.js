import React from 'react';
import './Reservar.css'; // Importar el archivo CSS correspondiente

const Reservar = () => {
  // Definir la función reserveSpot (reservar lugar) en el componente
  const reserveSpot = (lugarId) => {
    const lugar = document.getElementById(lugarId);
    lugar.classList.add('reserved');
    lugar.innerHTML = `Rmo hay<style"color:#FF6666"></style>`;
    alert(`Lugar ${lugarId} reservado`);
  };

  return (
    <div>
      <header>
        <nav>
          <a href="../html/inicio.html">Regresar a inicio</a>
        </nav>
      </header>
      <h1>Elige la fecha y hora</h1>
      <div className="datos">
        <form>
          <input type="date" />
          <br />
          <input type="time" />
        </form>
      </div>
      <h1>Elige tu lugar de estacionamiento</h1>
      <div className="grid-container">
        <div className="grid-item" id="A1">
          A1
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('A1')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="A2">
          A2
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('A2')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="A3">
          A3
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('A3')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="A4">
          A4
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('A4')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="A5">
          A5
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('A5')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="A6">
          A6
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('A6')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="A7">
          A7
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('A7')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="A8">
          A8
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('A8')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="A9">
          A9
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('A9')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="B1">
          B1
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('B1')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="B2">
          B2
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('B2')}>
            Reservar
          </button>
        </div>
        <div className="grid-item" id="B3">
          B3
          <br />
          <button className="reserve-button" onClick={() => reserveSpot('B3')}>
            Reservar
          </button>
        </div>
      </div>
    </div>
  );
};

export default Reservar;
